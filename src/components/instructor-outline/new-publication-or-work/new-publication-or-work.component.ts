import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService, ModalService, ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AdvancedFormComponent, EmptyValuePostProcessor } from '@universis/forms';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'universis-instructor-new-publication',
  templateUrl: './new-publication-or-work.component.html'
})
export class NewPublicationOrWorkComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  public loading = false;
  public lastError;
  @Input() execute: Observable<any>;
  @Input() data;
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  formConfig: any;
  private formLoadSubscription: Subscription;
  private formChangeSubscription: Subscription;
  @Input() formEditName;
  @Input() toastHeader;
  @Input() courseProperties;
  @Input() input;
  @Input() toastSuccess;


  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _translateService: TranslateService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
  }
  ngOnInit() {
    this.formComponent.data = this.data;
    this.formComponent.formName = this.formEditName;
    this.formChangeSubscription = this.formComponent.form.change.subscribe((event) => {
      if (Object.prototype.hasOwnProperty.call(event, 'isValid') && event.changed != null) {
        // enable or disable button based on form status
        this.okButtonDisabled = !event.isValid;
      }
    });
  }

  cancel(): Promise<any> {
    if (this.loading) {
      return;
    }
    // close
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  ok(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.loading = false;
      this._loadingService.showLoading();
      new EmptyValuePostProcessor().parse(this.formConfig, this.formComponent.form.formio.data);
      this.lastError = null;
      this.execute.subscribe(() => {
        this.loading = false;
        this._loadingService.hideLoading();
        if (this._modalService.modalRef) {
          this._modalService.modalRef.hide();
        }
        this._toastService.show(
          this._translateService.instant(this.toastHeader),
          this._translateService.instant(this.toastSuccess),
          true,
          3000
        );
        return resolve();
      }, (err) => {
        this.loading = false;
        this._loadingService.hideLoading();
        this.lastError = err;
        this.formComponent.form.onError(this._translateService.instant('E' + (err.status || 500) + '.message'));
        return reject(err);
      });
    });
  }

  onChange($event: any) {

  }

}

