import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ToastService, ErrorService, LoadingService, AppEventService } from '@universis/common';
import { QaService } from '../../../../qa.service';

@Component({
  selector: 'universis-instructor-work',
  templateUrl: './work.component.html'
})
export class WorkComponent implements OnInit {
  public isLoading = true;
  public works: any = [];

  constructor(private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _toastService: ToastService,
    private _errorService: ErrorService,
    private _loadingService: LoadingService,
    private _qaService: QaService,
    private _appEventService: AppEventService) { }

  async ngOnInit() {
    try {
      this.works = await this._qaService.getInstructorWorks();
      this.isLoading = false;
    } catch (err) {
      // use error service to navigate to error method
      return this._errorService.navigateToError(err);
    }
  }

  async getInitializedWorkTypes(newWork) {
    const workTypes = await this._qaService.getWorkTypes();
    const initializedWorks = workTypes.map(type => ({
      year: newWork.year,
      instructorOutline: newWork.instructorOutline,
      instructorWorkType: type,
      totalWork: type.id !== newWork.instructorWorkType.id ? 0 : newWork.totalWork,
    }));
    return initializedWorks;
  }

  async addOrEditWork(id) {
    // if an id is given, the corresponding work is loaded so that the user can update the record
    // else a new work will be created
    const data = id > 0 ? this.works.find(pub => pub.id === id) : null;
    this._qaService.showPublicationOrWorkModal({
      data: data,
      formName: 'InstructorWork',
      endpoint: 'InstructorWorks',
      toastBodySuccess: 'InstructorOutline.Work.SavedSuccess',
      reloadData: () => this._qaService.getInstructorWorks().then(works => {
        this.works = works;
        this.reloadTabsComponent();
      }),
      parseData: (newWork) => {
        // For every year, work records for all workTypes must be submitted. So, the fist time a work for a certain
        // year is created, ensure that zero-initialized work records for all workTypes are created as well.
        const currentWorks = this.works.filter(work => work.year === newWork.year);
        if (currentWorks.length === 0) {
          return this.getInitializedWorkTypes(newWork);
        }

        // if a work record with the same year and workType exists, new totalWork should be added to the old totalWork
        // if no id is given, we are editing an existing work thus this step should be skipped
        if (!id) {
          const sameWork = this.works.find(work =>
            work.year === newWork.year &&
            work.instructorWorkType.id === newWork.instructorWorkType.id);
          if (sameWork) {
            newWork.id = sameWork.id;
            newWork.totalWork += sameWork.totalWork;
          }
        }

        return Promise.resolve(newWork);
      }
    });
  }

  deleteWorks(works) {
    // console.log(works)
    this._loadingService.showLoading();
    this._context.model('qa/Instructors/me/InstructorWorks').save(works.map(work => ({ id: work.id, '$state': 4 })))
      .then(() => {
        this._loadingService.hideLoading();
        this.ngOnInit();
        this.reloadTabsComponent();
        this._toastService.show(
          this._translateService.instant('InstructorOutline.CompletedSuccess'),
          this._translateService.instant('InstructorOutline.Work.DeletedSuccess'),
          true,
          3000
        );
      })
      .catch(err => {
        // use error service to navigate to error method
        return this._errorService.navigateToError(err);
      });
  }

  /* triggers change in instructor-outline-tabs*/
  reloadTabsComponent() {
    this._appEventService.change.next({
      model: 'instructorOutline'
    });
  }

}
