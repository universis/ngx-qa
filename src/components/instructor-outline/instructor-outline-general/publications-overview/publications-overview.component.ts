import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService } from '@universis/common';

@Component({
  selector: 'universis-instructor-publications-overview',
  templateUrl: './publications-overview.component.html'
})
export class PublicationsOverviewComponent implements OnInit {
  public isLoading = true;
  public publications: any = [];

  constructor(private _context: AngularDataContext,
              private _errorService: ErrorService) { }

  async ngOnInit() {
    try {
      this.publications = await this._context.model('qa/Instructors/me/Publications')
        .asQueryable()
        .expand('publicationType($expand=locale($select=name);$select=id,name,locale)')
        .select('publicationType,count(publicationType) as count')
        .groupBy('publicationType')
        .take(-1)
        .getItems();

      this.isLoading = false;
    } catch (err) {
      // use error service to navigate to error method
      return this._errorService.navigateToError(err);
    }
  }

}
