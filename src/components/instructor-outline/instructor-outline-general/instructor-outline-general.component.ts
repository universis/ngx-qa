import { Component, OnInit } from '@angular/core';
import { AppEventService, ErrorService } from '@universis/common';
import { QaService } from '../../../qa.service';

@Component({
  selector: 'universis-instructor-outline-general',
  templateUrl: './instructor-outline-general.component.html'
})
export class InstructorOutlineGeneralComponent implements OnInit {
  public outlineId;
  public isLoading = true;
  public worksSum: any;
  public publicationsCount: any;
  public instructorOutlineReports: any;

  constructor(private _appEventService: AppEventService,
              private _qaService: QaService,
    private _errorService: ErrorService) { }

  ngOnInit() {
    // load data from instructor-outline-tabs-component
    this._appEventService.change.subscribe( async (x) => {
      this.publicationsCount = x.publicationsCount;
      this.worksSum = x.worksSum;
      this.outlineId = x.outlineId;
      this.isLoading = false;
      this.instructorOutlineReports = await this._qaService.getReportTemplates('InstructorOutline');
    });
  }

  async printInstructorOutline(report) {
    try {
      // print report by passing report parameters
      const blob = await this._qaService.printReport(report.id, {
        ID: this.outlineId,
        REPORT_USE_DOCUMENT_NUMBER: false
      });
      const objectUrl = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = objectUrl;
      const name = `${report.name}.pdf`;
      a.download = name;
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, name);
      } else {
        a.click();
      }
      window.URL.revokeObjectURL(objectUrl);
      a.remove(); // remove the element
    } catch (err) {
      console.log(err);
      return this._errorService.navigateToError(err);
    }
  }

}
