import { Component, OnInit } from '@angular/core';
import {ErrorService, LoadingService} from '@universis/common';
import {QaService} from '../../../qa.service';

@Component({
  selector: 'universis-course-evaluations-list',
  templateUrl: './course-evaluations-list.component.html',
  styleUrls: ['./course-evaluations-list.component.scss']
})
export class CourseEvaluationsListComponent implements OnInit {

  public evaluations: any = [];
  public years: any = [];
  public periods: any = [];
  public evaluationsResultsCount: 0;
  public isLoading = true;   // Only if data is loaded
  public searchText = '';
  public searchTextYear = '';
  public searchTextPeriod = '';

  constructor( private _loadingService: LoadingService,
               private _errorService: ErrorService,
               private _qaService: QaService
  ) { }

  ngOnInit() {
    this.isLoading = false;
    this.loadSearchFields();
  }

  async loadSearchFields() {
    this.isLoading = true;
    try {
      this._loadingService.showLoading();
      this.years = await this._qaService.getCourseClassEvaluationsYears();
      this.periods = await this._qaService.getCourseClassEvaluationsPeriods();
      this.isLoading = false;
    } catch (err) {
      console.error(err);
      return this._errorService.navigateToError(err);
    } finally {
      this.isLoading = false;
      this._loadingService.hideLoading();
    }
  }

  async getData(searchText: any, searchTextYear: any, searchTextPeriod: any) {
    this.isLoading = true;
    try {
      this._loadingService.showLoading();
      const evaluations = await this._qaService.getInstructorEvaluations(searchText, searchTextYear, searchTextPeriod);
      // const evaluationsResultsCount = await this._qaService.getEvaluationEventAttributes(evaluations.id);
      // .evaluationsResultsCount = evaluationsResultsCount.length;
      this.evaluations = evaluations.map((x) => {
        x.url = `/courses/${x.courseClassInstructor.courseClass.course.id}/${x.courseClassInstructor.courseClass.year.id}/${x.courseClassInstructor.courseClass.period.id}/evaluation`;
        return x;
      });
      // get result for any evaluation
      for (const x of this.evaluations) {
        x.results = await this._qaService.getEvaluationEventTokens(x.id);
      }
      this.isLoading = false;
    } catch (err) {
      console.error(err);
      return this._errorService.navigateToError(err);
    } finally {
      this.isLoading = false;
      this._loadingService.hideLoading();
    }
  }
}
