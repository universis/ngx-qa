import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AngularDataContext} from '@themost/angular';
import {ErrorService, LoadingService} from '@universis/common';
import {QaService} from '../../qa.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'universis-course-evaluation',
  templateUrl: './course-evaluation.component.html'
})
export class CourseEvaluationComponent implements OnInit {

  private dataSubscription: Subscription;
  private courseClass: any;
  public courseClassReports: any;
  public data: any;

  constructor(private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _activatedRoute: ActivatedRoute,
              private _qaService: QaService,
              private _errorService: ErrorService
              ) { }

  async ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(async params => {
      this._loadingService.showLoading();
      this.courseClass = this.courseClass || params.courseClass;
      try {
        // Fetch main Evaluation event Info
        this.data = await this._qaService.getCourseClassInstructorEvaluationEvent(this.courseClass.id || this.courseClass);
        this.courseClassReports = await this._qaService.getReportTemplates('ClassInstructorEvaluationEvent');
        this._loadingService.hideLoading();
      } catch (err) {
        this._loadingService.hideLoading();
        // use error service navigate to error method
        return this._errorService.showError(err);
      }
    });
  }

  async printCourseOutline(report, parameter_id) {
    try {
      // print report by passing report parameters
      const blob = await this._qaService.printReport(report.id, {
        ID: parameter_id,
        REPORT_USE_DOCUMENT_NUMBER: false
      });
      const objectUrl = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = objectUrl;
      const name = `${report.name}.pdf`;
      a.download = name;
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, name);
      } else {
        a.click();
      }
      window.URL.revokeObjectURL(objectUrl);
      a.remove(); // remove the element
    } catch (err) {
      return err;
    }
  }
}
