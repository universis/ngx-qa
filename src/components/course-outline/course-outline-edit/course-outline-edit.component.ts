import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigurationService, ErrorService, LoadingService, ToastService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';
import ISO6391 from 'iso-639-1';
import { QaService } from '../../../qa.service';
import { FormioSubmissionCallback } from 'angular-formio';

@Component({
  selector: 'universis-course-outline-edit',
  templateUrl: './course-outline-edit.component.html',
  styleUrls: ['./course-outline-edit.component.css']
})
export class CourseOutlineEditComponent implements OnInit, OnDestroy {
  @Input() continueLink: any;
  @Input() courseClass: any;
  @ViewChild('formEditGeneral') formEditGeneral: AdvancedFormComponent;
  private formLoadSubscription: Subscription;
  public srcEditGeneral = 'courseOutline';
  public data: any;


  constructor(
    private _toastService: ToastService,
    private _qaService: QaService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _loadingService: LoadingService,
    private _configurationService: ConfigurationService,
    private _errorService: ErrorService) { }

  ngOnInit() {
    this.formLoadSubscription = this.formEditGeneral.form.formLoad.subscribe(() => {
      if (this.formEditGeneral.form.options) {
        // hide formio alerts errors
        this.formEditGeneral.form.options.disableAlerts = true;
        this.configBeforeSubmitHook();
      }
    });

    this.courseClass = this.courseClass || this._activatedRoute.parent.snapshot.data.courseClass;
    this._loadingService.showLoading();
    // Fetch the Course Outline of the specified course-class
    this._qaService.getCourseOutline(this.courseClass.id).then(outline => {
      this.configFormioData(outline).refreshFormioData();
      this._loadingService.hideLoading();
    }).catch(err => {
      // show error
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '../'
      });
    });
  }

  configFormioData(outline) {
    const data = outline || { 'courseClass': this.courseClass };
    const departmentLocales = this.courseClass.department.locales.map(locale => locale.inLanguage);
    const defaultLocale = this._configurationService.settings.i18n.defaultLocale;

    // Ensure that outline locales and department locales both refer to the same inLanguages
    data.locales = this._configurationService.settings.i18n
      && this._configurationService.settings.i18n.locales
      && this._configurationService.settings.i18n.locales.map(locale => {
        const localeCode = ISO6391.getCode(locale);
        const existentLocale = Array.isArray(data.locales) && data.locales.find(loc => loc.inLanguage === localeCode);
        return existentLocale || { inLanguage: localeCode };
      }).filter(locale => locale.inLanguage !== defaultLocale && departmentLocales.includes(locale.inLanguage))
      || [];
    this.data = data;
    return this;
  }

  /* This hook will be executed before the data is submitted to the server. It ensures that $state 4 is assigned to
  teachingMethods and ictTypes that were previously saved but were removed in this session */
  configBeforeSubmitHook() {
    this.formEditGeneral.form.options.hooks.beforeSubmit = (submission: any, execute: FormioSubmissionCallback) => {
      if (this.data.id) {
        // make a deep copy of the submission object so that the form's original data aren't altered
        submission.data = JSON.parse(JSON.stringify(submission.data));
        const data = submission.data;
        const keysToCheck = ['teachingMethods', 'ictTypes'];

        if (data && this.data) {
          for (const key of keysToCheck) {
            const oldData = this.data[key] || [];
            const newData = data[key] || [];

            for (const entry of oldData) {
              if (!newData.find(newEntry => newEntry.id === entry.id)) {
                entry.$state = 4;
                newData.push(entry);
              }
            }
          }
        }
      }
      // call formio's callback with the submission object to trigger server submission
      execute(null, submission);
    };
  }

  refreshFormioData() {
    this.formEditGeneral.refreshForm.emit({
      submission: {
        data: this.data
      }
    });
  }

  onCompletedSubmission(event: any) {
    this._toastService.show(event.toastMessage.title, event.toastMessage.body);
    this._router.navigate(['../../outline'], { relativeTo: this._activatedRoute });
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
  }
}
